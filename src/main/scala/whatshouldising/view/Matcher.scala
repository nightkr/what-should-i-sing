package whatshouldising.view

import monix.reactive.Observable
import outwatch._
import outwatch.dsl._
import whatshouldising.Accounts
import whatshouldising.Artist
import whatshouldising.SingaSongSource
import whatshouldising.SpotifySongSource
import ScalaCSSOutwatch._
import org.scalajs.dom.raw.HTMLInputElement
import outwatch.reactive.handlers.monix.Handler
import monix.execution.Scheduler
import colibri.ext.monix._

object Matcher {
  def main(
      accounts: Accounts,
      spotifySource: SpotifySongSource,
      singaSource: SingaSongSource
  )(implicit s: Scheduler) =
    for {
      spotifyTimeRange <- Handler.create[SpotifySongSource.TimeRange](
        SpotifySongSource.TimeRange.LongTerm
      )
      showMissingArtists <- Handler.create[Boolean](false)
    } yield div(
      h2(
        "How far back should we look at your Spotify history?"
      ),
      ul(
        SpotifySongSource.TimeRange.all
          .map(
            range =>
              li(
                label(
                  input(
                    `type` := "radio",
                    checked <-- spotifyTimeRange.map(
                      _ == range
                    ),
                    onClick
                      .use(range) --> spotifyTimeRange
                  ),
                  range.displayName
                )
              )
          )
      ),
      h2("Should we show missing artists?"),
      p(
        label(
          input(
            `type` := "checkbox",
            checked <-- showMissingArtists,
            onChange.map(_.target.asInstanceOf[HTMLInputElement].checked) --> showMissingArtists
          ),
          "Yes"
        )
      ),
      h2("You should sing this!"),
      spotifyTimeRange.map(
        timeRange =>
          showSuggestions(
            timeRange,
            spotifySource,
            singaSource,
            showMissingArtists
          )
      )
    )

  def showSuggestions(
      timeRange: SpotifySongSource.TimeRange,
      spotifySource: SpotifySongSource,
      singaSource: SingaSongSource,
      showMissingArtists: Observable[Boolean]
  )(implicit s: Scheduler) = table(
    Styles.matcherTable,
    tr(Styles.matcherHeaderRow, th("Artist"), th("Songs")),
    spotifySource
      .TopArtists(timeRange)
      .artists
      .zipWithIndex
      .map {
        case ((artist, i)) =>
          tr(
            Styles.matcherArtistRow,
            showArtist(i + 1, artist, singaSource, showMissingArtists)
          )
      }
      .map[ChildCommand](ChildCommand.Append(_))
  )

  def showArtist(
      placement: Long,
      artist: Artist,
      singaSource: SingaSongSource,
      showMissingArtists: Observable[Boolean]
  )(implicit s: Scheduler) =
    singaSource
      .songsByArtist(artist)
      .publishSelector(
        (songs) =>
          Observable(
            VDomModifier(
              showMissingArtists
                .combineLatestMap(false +: songs.isEmpty)(
                  (showEmpty, isEmpty) => !showEmpty && isEmpty
                )
                .map[VDomModifier](
                  if (_) Styles.hidden
                  else VDomModifier.empty
                ),
              td(placement, ". ", artist.name),
              td(
                Styles.matcherArtistSongsCell,
                ul(
                  Styles.matcherArtistSongList,
                  songs
                    .map(_.name)
                    // .defaultIfEmpty((("Sorry, couldn't find anything")))
                    .map(li(_))
                    .map[ChildCommand](ChildCommand.Append(_))
                ),
                div("(loading more songs)") +: songs
                  .map(_ => ())
                  .defaultIfEmpty(())
                  .last
                  .map(_ => VDomModifier.empty)
              )
            )
          )
      )
}
