package whatshouldising.view

import outwatch.dsl._

object Legal {
  val copyrightFooter = p(
    a(
      "🄯",
      href := "https://gitlab.com/teozkr/what-should-i-sing"
    ),
    " 2020 Teo Klestrup Röijezon"
  )
  val disclaimerFooter =
    p(
      "Not endorsed by Singa or Spotify, nor should it be considered an endorsement of either"
    )
  val privacyPolicy = div(
    p(
      "We may store logs indicating that and when you have accessed the site, but we do not store any information about your listening habits. ",
      "In fact, your listening habits or Spotify credentials are never even sent to our servers. ",
      "However, your Spotify top artists are transmitted to ",
      b("Singa"),
      " in order to deliver the service."
    )
  )
}
