package whatshouldising.view

import scalacss.{StyleA, StyleSheet}
import scalacss.internal.{Css, Renderer}
import outwatch.Render
import outwatch.{VDomModifier, VNode}

object StylesMeta {
  val defaults = scalacss.devOrProdDefaults
}

import StylesMeta.defaults._

object Styles extends StyleSheet.Inline {
  import dsl._

  style(unsafeRoot("body")(margin.`0`, boxSizing.borderBox))

  val mobile = media.maxWidth(480.px)

  val hidden = style(display.none.important)

  val appContentWidth = mixin(
    padding(8.px),
    maxWidth(1000.px),
    marginLeft.auto,
    marginRight.auto
  )

  val appBody = style(
    // display.flex,
    // flexDirection.column,
    // alignItems.center
  )

  val header = style(
    backgroundColor(lightgray),
    margin.`0`
    // padding(8.px)
    // alignSelf.normal
  )
  val headerContent =
    style(appContentWidth, display.flex, alignItems.center)
  val headerTitle = style(margin.`0`, flexGrow(1))
  val headerNav   = style(listStyleType := "none", margin.`0`)

  val mainContent = style(appContentWidth)

  val matcherTable     = style(width(100.%%), borderCollapse.collapse)
  val matcherHeaderRow = style(mobile(hidden))
  val matcherArtistRow = style(
    borderTopColor.black,
    borderTopWidth(1.px),
    borderTopStyle.solid,
    mobile(display.block)
  )
  val matcherArtistSongsCell = style(
    mobile(display.block)
  )
  val matcherArtistSongList = style(
    margin.`0`
  )

  val footer = style(appContentWidth)
}

object ScalaCSSOutwatch {
  import outwatch.dsl.{`class`, htmlTag}

  implicit val styleARender: Render[StyleA] =
    new Render[StyleA] {
      def render(value: scalacss.StyleA): VDomModifier =
        `class` := value.htmlClass
    }

  implicit def vnodeRenderer(
      implicit strRenderer: Renderer[String]
  ): Renderer[VNode] = new Renderer[VNode] {
    override def apply(css: Css): VNode =
      htmlTag("style")(strRenderer(css))
  }
}
