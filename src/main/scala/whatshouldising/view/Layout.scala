package whatshouldising.view

import outwatch._
import outwatch.dsl._
import whatshouldising.view.util.Location
import StylesMeta.defaults._
import ScalaCSSOutwatch._
import monix.reactive.Observable
import whatshouldising.SpotifyOAuth
import whatshouldising.SingaSongSource
import whatshouldising.Accounts
import whatshouldising.SpotifySongSource
import colibri.ext.monix._
import monix.execution.Scheduler

object Layout {
  def layout(
      content: VDomModifier,
      accounts: Accounts,
      spotifyOAuth: SpotifyOAuth
  )(
      implicit l: Location,
      s: Scheduler
  ) =
    div(
      Styles.renderA[VNode],
      Styles.appBody,
      headerBar(accounts, spotifyOAuth),
      div(Styles.mainContent, content),
      footer(
        Styles.footer,
        Legal.copyrightFooter,
        Legal.disclaimerFooter,
        p("View our ", a(Page.PrivacyPolicy.link, "privacy policy"))
      )
    )

  def headerBar(accounts: Accounts, spotifyOAuth: SpotifyOAuth)(
      implicit s: Scheduler
  ) = header(
    Styles.header,
    div(
      Styles.headerContent,
      h1(Styles.headerTitle, "What Should I Sing?!"),
      navMenu(accounts, spotifyOAuth)
    )
  )

  def navMenu(accounts: Accounts, spotifyOAuth: SpotifyOAuth)(
      implicit s: Scheduler
  ) = ul(
    Styles.headerNav,
    li(
      accounts.active.map(
        if (_)
          a(
            href := "javascript:{}",
            onClick.preventDefault.use(()) --> accounts.logOut,
            "Log out"
          )
        else a(href := spotifyOAuth.loginRedirect, "Log in with Spotify")
      )
    )
  )

  def currentPage(
      accounts: Accounts,
      spotifySource: Observable[Option[SpotifySongSource]],
      spotifyOAuth: SpotifyOAuth,
      singaSource: SingaSongSource
  )(implicit l: Location, s: Scheduler) = Page.current.map[VDomModifier] {
    case Page.Home =>
      homePage(accounts, spotifySource, spotifyOAuth, singaSource)
    case Page.AuthCallbackSpotify =>
      VDomModifier(
        "logging you into spotify!",
        // This is conceptually "just" an IO, but Outwatch tries to run all IOs synchronously atm
        // See https://github.com/OutWatch/outwatch/pull/328
        spotifyOAuth
          .loginCallback()
          .map(_ => VDomModifier.empty)
      )
    case Page.PrivacyPolicy =>
      div(
        ul(li(a(Page.Home.link, "Go back"))),
        Legal.privacyPolicy
      )
    case Page.NotFound(_) => "not found"
  }

  def homePage(
      accounts: Accounts,
      spotifySource: Observable[Option[SpotifySongSource]],
      spotifyOAuth: SpotifyOAuth,
      singaSource: SingaSongSource
  )(implicit s: Scheduler) =
    spotifySource.map[VDomModifier] {
      case None => landingPage(spotifyOAuth)
      case Some(spotifySource) =>
        Matcher.main(accounts, spotifySource, singaSource)
    }

  def landingPage(spotifyOAuth: SpotifyOAuth) =
    div(
      p(
        "Ever got annoyed at a karaoke bar because you couldn't find any songs you liked? ",
        em("What Should I Sing?!"),
        " is here to help! ",
        "It shows you which of your most streamed artists are available!"
      ),
      h2("Supported services"),
      p(
        "The following services are currently supported by WSIS. ",
        a(
          href := "https://gitlab.com/teozkr/what-should-i-sing",
          "Contributions are welcome."
        )
      ),
      h3("Karaoke services"),
      p(ul(li("Singa"))),
      h3("Music taste sources"),
      p(ul(li("Spotify")))
    )
}
