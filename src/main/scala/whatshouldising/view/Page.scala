package whatshouldising.view

import outwatch._
import outwatch.dsl._
import whatshouldising.view.util.Location
import colibri.ext.monix._

sealed trait Page {
  def path: String
  def link(implicit l: Location): VDomModifier =
    VDomModifier(
      href := path,
      onClick.preventDefault.use(path) --> l.path
    )
}
object Page {
  case object Home extends Page {
    override def path: String = "/"
  }
  case object PrivacyPolicy extends Page {
    override def path: String = "/policy/privacy"
  }
  case object AuthCallbackSpotify extends Page {
    override def path: String = "/auth/callback/spotify"
  }
  case class NotFound(path: String) extends Page

  def fromPath(path: String): Page = path match {
    case "/"                      => Home
    case "/auth/callback/spotify" => AuthCallbackSpotify
    case "/policy/privacy"        => PrivacyPolicy
    case _                        => NotFound(path)
  }

  def current(implicit l: Location) = l.path.map(fromPath)
}
