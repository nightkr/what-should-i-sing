package whatshouldising.view.util

import cats.effect.IO
import cats.effect.Resource
import colibri.ext.monix._
import monix.execution.Ack
import monix.execution.Scheduler
import monix.reactive.Observer
import monix.reactive.OverflowStrategy
import monix.reactive.subjects.ConcurrentSubject
import org.scalajs.dom
import outwatch.dsl.events
import outwatch.reactive.handlers.monix.{Handler, ProHandler}
import scala.concurrent.Future
import scala.scalajs.js

trait Location {
  val path: Handler[String]
  val base: String
  val hash: String

  def pushPath(path: String): IO[Unit]
  def replacePath(path: String): IO[Unit]
}

object Location {
  @js.native
  private trait HasBaseURI extends js.Object {
    def baseURI: String = js.native
  }

  def history(implicit scheduler: Scheduler): Resource[IO, Location] =
    Resource {
      IO {
        val updatePath =
          ConcurrentSubject.behavior[Unit]((), OverflowStrategy.DropOld(2))
        val currentPath =
          updatePath.map(_ => dom.window.document.location.pathname)

        // Update on DOM history events
        val cancelUpdates =
          events.window.onPopState
            .map(_ => ())
            .subscribe[Observer](updatePath)

        val impl = new Location {
          val path: Handler[String] =
            ProHandler(
              new Observer[String] {
                override def onNext(elem: String): Future[Ack] =
                  pushPath(elem).map(_ => Ack.Continue).unsafeToFuture
                override def onError(ex: Throwable): Unit = throw ex
                override def onComplete(): Unit           = {}
              },
              currentPath
            )
          val base = dom.window.document.asInstanceOf[HasBaseURI].baseURI
          val hash = dom.window.document.location.hash

          def pushPath(path: String): IO[Unit] =
            IO {
              dom.window.history.pushState(null, null, path)
              assert(updatePath.onNext(()) == Ack.Continue)
            }

          def replacePath(path: String): IO[Unit] =
            IO {
              dom.window.history.replaceState(null, null, path)
              assert(updatePath.onNext(()) == Ack.Continue)
            }
        }

        (impl, IO(cancelUpdates.cancel()))
      }
    }
}
