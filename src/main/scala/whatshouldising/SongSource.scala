package whatshouldising

import monix.reactive.Observable

case class Artist(name: String)
case class Song(name: String)

trait ArtistSource {
  def artists: Observable[Artist]
}

trait SongSource {
  def songsByArtist(artist: Artist): Observable[Song]
}
