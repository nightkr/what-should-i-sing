package whatshouldising

import cats.effect.IO
import colibri.ext.monix._
import colibri.ext.monix.ops._
import io.circe.generic.JsonCodec
import io.circe.parser._
import io.circe.syntax._
import java.time.Instant
import outwatch.reactive.handlers.monix.Handler
import outwatch.util.SessionStorage

@JsonCodec
case class OAuthToken(
    accessToken: String,
    tokenType: String,
    validUntil: Instant
) {
  val valid: IO[Boolean] = IO(Instant.now().isBefore(validUntil))
}

trait Accounts {
  def spotify: Handler[Option[OAuthToken]]

  def active = spotify.map(_.nonEmpty)
  def logOut = spotify.contramap[Unit](_ => None)
}

object Accounts {
  def apply(): IO[Accounts] =
    for {
      spotify_ <- SessionStorage
        .handler[IO]("accounts.spotify")
        .map(liftSubjectToMonix(_))
      // spotify_ <- Handler.create[Option[OAuthToken]](None)
    } yield new Accounts {
      lazy val spotify =
        spotify_
          .mapSubject[Option[OAuthToken]](_.map(_.asJson.noSpaces))(
            _.map(decode[OAuthToken]).map(_.toTry.get)
          )
          .transformObservable(
            _.mapEvalF(
              token =>
                token
                  .fold(IO.pure(false))(_.valid)
                  .map(valid => token.filter(_ => valid))
            )
          )
    }

  private def liftSubjectToMonix[I, O](
      subject: colibri.ProSubject[I, O]
  ): MonixProSubject[I, O] =
    monixCreateProSubject.from(
      subject: colibri.Observer[I],
      subject: colibri.Observable[O]
    )
}
