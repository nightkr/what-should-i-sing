package whatshouldising

import outwatch.util.Http
import cats.effect.IO
import java.time.Instant
import cats.effect.ContextShift
import monix.reactive.Observable
import io.circe.generic.JsonCodec
import io.circe.scalajs.decodeJs
import whatshouldising.view.util.Location
import whatshouldising.view.Page

class SpotifySongSource(acct: OAuthToken)(implicit cs: ContextShift[IO]) {
  private val apiBase = "https://api.spotify.com"

  @JsonCodec
  case class SpotifyListPage[T](next: Option[String], items: Seq[T])
  @JsonCodec
  case class SpotifyArtist(name: String) {
    def toArtist = Artist(name = name)
  }

  case class TopArtists(timeRange: SpotifySongSource.TimeRange)
      extends ArtistSource {
    val artists = Observable
      .unfoldEvalF(
        Option(
          s"$apiBase/v1/me/top/artists?limit=49&time_range=${timeRange.spotifyName}"
        )
      ) {
        case Some(url) =>
          Http
            .single(
              Http.Request(
                url,
                responseType = "json",
                headers = Map(
                  "Authorization" -> s"${acct.tokenType} ${acct.accessToken}"
                )
              ),
              Http.Get
            )
            .map(_.response)
            .map(decodeJs[SpotifyListPage[SpotifyArtist]])
            .map(_.toTry.get)
            .map(page => Some((page.items.map(_.toArtist), page.next)))
        case None => IO.pure(None)
      }
      .concatMap(Observable.apply)
  }
}

object SpotifySongSource {
  sealed trait TimeRange {
    val spotifyName: String
    val displayName: String
  }
  object TimeRange {
    case object LongTerm extends TimeRange {
      val spotifyName = "long_term"
      val displayName = "All of it!"
    }
    case object MediumTerm extends TimeRange {
      val spotifyName = "medium_term"
      val displayName = "Last 6 months"
    }
    case object ShortTerm extends TimeRange {
      val spotifyName = "short_term"
      val displayName = "Last 4 weeks"
    }
    val all = Seq[TimeRange](LongTerm, MediumTerm, ShortTerm)
  }
}

class SpotifyOAuth(accounts: Accounts)(
    implicit location: Location,
    cs: ContextShift[IO]
) {
  val scopes      = Seq("user-top-read")
  val clientId    = "1a300c384e6f4bd6b92aafe7d4d4fa2b"
  val callbackUrl = location.base.stripSuffix("/") + Page.AuthCallbackSpotify.path
  val loginRedirect = {
    val scopesStr = scopes.mkString(" ")
    s"https://accounts.spotify.com/authorize?response_type=token&client_id=$clientId&redirect_uri=$callbackUrl&scope=${scopesStr}"
  }
  def loginCallback(): IO[Unit] =
    IO.fromFuture(IO {
        val hashParts = location.hash
          .stripPrefix("#")
          .split("&")
          .map(_.split("="))
          .map { case Array(key, value) => (key, value) }
          .toMap
        val token = OAuthToken(
          accessToken = hashParts("access_token"),
          tokenType = hashParts("token_type"),
          validUntil = Instant.now().plusSeconds(hashParts("expires_in").toInt)
        )
        accounts.spotify.onNext(Some(token))
      })
      .flatMap(_ => location.replacePath(Page.Home.path))
}
