package whatshouldising

import cats.effect.ExitCode
import cats.effect.IO
import cats.effect.IOApp
import colibri.ext.monix._
import monix.execution.Scheduler.Implicits.global
import outwatch._
import whatshouldising.view.Layout
import whatshouldising.view.util.Location

object WhatShouldISing extends IOApp {
  def run(args: List[String]): IO[ExitCode] =
    Location.history.allocated
      .flatMap {
        // Don't cancel, since OutWatch.renderInto finishes after the first render,
        // but we need to keep the resource for the lifetime of the application
        case ((location_, _cancel)) =>
          implicit val location = location_
          // implicit val ioCs     = IO.contextShift(scheduler)
          for {
            accounts <- Accounts()
            spotifyOAuth = new SpotifyOAuth(accounts)
            spotifySource = accounts.spotify.map(
              _.map(new SpotifySongSource(_))
            )
            singaSource = new SingaSongSource
            _ <- OutWatch
              .renderInto[IO](
                "#app",
                Layout.layout(
                  Layout.currentPage(
                    accounts,
                    spotifySource,
                    spotifyOAuth,
                    singaSource
                  ),
                  accounts,
                  spotifyOAuth
                )
              )
          } yield ExitCode.Success
      }
}
