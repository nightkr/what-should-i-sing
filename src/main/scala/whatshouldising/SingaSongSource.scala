package whatshouldising

import monix.reactive.Observable
import outwatch.util.Http
import cats.effect.ContextShift
import cats.effect.IO
import io.circe.generic.JsonCodec
import io.circe.scalajs.decodeJs
import scala.scalajs.js.URIUtils

class SingaSongSource(implicit cs: ContextShift[IO])
    extends ArtistSource
    with SongSource {
  @JsonCodec
  case class SingaListPage[T](next: Option[String], results: Seq[T])
  @JsonCodec
  case class SingaArtist(id: Int, name: String, songs: String) {
    def toArtist = Artist(name = name)
  }
  @JsonCodec
  case class SingaSong(name: String) {
    def toSong = Song(name = name)
  }
  @JsonCodec
  case class SingaList[T](items: Seq[T])
  @JsonCodec
  case class SingaSearchResults(artists: Option[SingaList[SingaArtist]])

  private val apiBase = "https://api.sin.ga/v1.4"

  private def artistsPage(
      url: String
  ): IO[SingaListPage[SingaArtist]] =
    Http
      .single(Http.Request(url, responseType = "json"), Http.Get)
      .map(_.response)
      .map(decodeJs[SingaListPage[SingaArtist]])
      .map(_.toTry.get)
  def artists: Observable[Artist] =
    Observable
      .unfoldEvalF(Option(s"$apiBase/artists/?page_size=2000")) {
        case Some(url) =>
          artistsPage(url)
            .map(page => (page.results.map(_.toArtist), page.next))
            .map(Some(_))
        case None => IO.pure(None)
      }
      .concatMap(Observable.apply)

  def artistByName(name: String): IO[Option[SingaArtist]] = {
    val urlencodedName = URIUtils.encodeURIComponent(name)
    Http
      .single(
        Http.Request(
          s"$apiBase/search/?type=artists&search=$urlencodedName",
          responseType = "json"
        ),
        Http.Get
      )
      .map(_.response)
      .map(decodeJs[SingaSearchResults])
      .map(_.toTry.get.artists.flatMap(_.items.find(_.name == name)))
  }
  def songsBySingaArtistPage(url: String): IO[SingaListPage[SingaSong]] =
    Http
      .single(Http.Request(url, responseType = "json"), Http.Get)
      .map(_.response)
      .map(decodeJs[SingaListPage[SingaSong]])
      .map(_.toTry.get)
  def songsByArtist(artist: Artist): Observable[Song] =
    Observable
      .from(artistByName(artist.name))
      .switchMap {
        case None => Observable()
        case Some(artist) =>
          Observable.unfoldEvalF(Option(artist.songs)) {
            case None => IO.pure(None)
            case Some(url) =>
              songsBySingaArtistPage(url)
                .map(page => (page.results.map(_.toSong), page.next))
                .map(Some(_))
          }
      }
      .concatMap(Observable.apply)
}
